"""Install package sets."""

from __future__ import annotations

import sys
from logging import getLogger
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    import argparse

from . import spack
from .available import query_package_sets, query_release_branches
from .config import Config, require_valid_software_root
from .git_helper import initialise_or_update_repository
from .log_handler import init_file_logger
from .util import abort, initialise_dir

logger = getLogger(__name__)


def install(args: argparse.Namespace) -> None:
    """Install package set(s)."""
    software_release: str = args.software_release
    package_sets: list[str] = args.package_set

    # print(args)

    require_valid_software_root(command="install")
    spack_environments_branch = validate_software_release_and_package_sets(
        software_release, package_sets
    )

    config = Config()
    install_dir = args.install_dir or software_release
    config.resolve_paths(install_dir)
    config.source_cache_root = args.source_cache_path

    init_file_logger()

    spack.ensure_no_active_spack()

    initialise_dir(config.release_root, "release")
    if not args.no_pull:
        initialise_or_update_repository(
            config.spack_environments_repository,
            config.spack_environments_root,
            args.git_protocol,
            spack_environments_branch,
        )
    spack_branch = find_spack_branch(spack_environments_branch)

    initialise_dir(config.microarch_root, "microarch")
    if not args.no_pull:
        initialise_or_update_repository(
            config.spack_repository, config.spack_root, args.git_protocol, spack_branch
        )

    spack.bootstrap()
    spack.configure_spack_mirrors(args.disable_binary_cache)
    spack.find_system_compiler(args.system_compiler)
    spack.update_custom_spack_config()

    installation_summary = {
        package_set: ("Installation pending", logger.warning)
        for package_set in package_sets
    }
    installation_successfull = True
    for package_set in package_sets:
        try:
            logger.info("installing package set '%s'", package_set)
            spack.install_package_set(package_set)
        except spack.SpackConcretizationError:
            installation_summary[package_set] = (
                "Environment concretization failed",
                logger.error,
            )
            installation_successfull = False
            if args.fail_fast:
                break
        except spack.SpackInstallationError:
            installation_summary[package_set] = ("Installation failed", logger.error)
            installation_successfull = False
            if args.fail_fast:
                break
        except spack.ModuleGenerationError:
            installation_summary[package_set] = (
                "Module file generation failed",
                logger.error,
            )
            installation_successfull = False
            if args.fail_fast:
                break
        except spack.CondaEnvError:
            installation_summary[package_set] = (
                "Conda env creation failed",
                logger.error,
            )
            installation_successfull = False
            if args.fail_fast:
                break
        else:
            installation_summary[package_set] = ("Installation succeeded", logger.info)

    spack.update_caches(False)

    logger.info("%s\nInstallation summary\n%s", "=" * 20, "=" * 20)
    for package_set, (message, log_callback) in installation_summary.items():
        log_callback("%s: %s", package_set, message)

    if not installation_successfull:
        sys.exit(1)


def validate_software_release_and_package_sets(
    software_release: str, package_sets: list[str]
) -> str:
    """Verify that the software release exists and provides all package sets."""
    # query package set terminates the program if software_release is invalid
    available_package_sets = query_package_sets(software_release)

    missing_package_sets = []
    for package_set in package_sets:
        if package_set not in available_package_sets:
            missing_package_sets.append(package_set)

    if missing_package_sets:
        message_list = [
            "The following package sets are not available for release"
            f" '{software_release}':"
        ]
        for package_set in missing_package_sets:
            message_list.append(f"  - {package_set}")
        abort("\n".join(message_list))

    return (
        f"releases/{software_release}"
        if software_release in query_release_branches()
        else software_release
    )


def find_spack_branch(spack_environments_branch: str) -> str:  # type: ignore[return]
    """Find the name of the spack branch for a given spack_environments branch."""
    config = Config()
    with (config.spack_environments_root / "spack_setup.sh").open() as f:
        for line in f:
            if line.startswith("spack_branch"):
                # format: spack_branch='mpsd/v0.21_develop\n'
                return line.split("=")[1].strip().strip("'")

    abort(
        "Could not determine correct spack branch corresponding to "
        f"'{spack_environments_branch}' branch in spack-environments repository"
    )
