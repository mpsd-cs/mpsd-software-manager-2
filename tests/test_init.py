"""Test interna of mpsd-software init."""

import argparse
import logging
from contextlib import AbstractContextManager
from pathlib import Path
from typing import Callable

import pytest
from mpsd_software_manager.config import MPSD_SOFTWARE_ROOT_MARKER
from mpsd_software_manager.init import init


def test_init(
    cd: Callable[[Path], AbstractContextManager[None]],
    tmp_path: Path,
    caplog: pytest.LogCaptureFixture,
) -> None:
    """Test initialisation of MPSD software root."""
    caplog.set_level(logging.INFO)
    # empty namespace for init (required for mypy)
    args = argparse.Namespace()

    # ensure we start in a clean directory
    assert not (tmp_path / MPSD_SOFTWARE_ROOT_MARKER).exists()

    # initialise tmp_path as MPSD software root
    caplog.clear()
    with cd(tmp_path):
        init(argparse.Namespace())
    assert (tmp_path / MPSD_SOFTWARE_ROOT_MARKER).is_file()
    assert f"Directory '{tmp_path}' initialised as MPSD software root" in caplog.text

    # rerun in the already initialised directory
    caplog.clear()
    with cd(tmp_path):
        init(args)
    assert (tmp_path / MPSD_SOFTWARE_ROOT_MARKER).is_file()
    assert f"Directory '{tmp_path}' is already initialised" in caplog.text

    # rerun in subdirectory
    tmp_subdir = tmp_path / "subdir"
    tmp_subdir.mkdir()

    caplog.clear()
    with cd(tmp_subdir):
        init(args)
    assert (tmp_subdir / MPSD_SOFTWARE_ROOT_MARKER).is_file()
    assert f"Directory '{tmp_subdir}' initialised as MPSD software root" in caplog.text
