"""Test spack install steps and status reports with minimalist toolchains.

The individual tests in this module depend on each other as we keep adding
to the same spack instance.
"""

from __future__ import annotations

import logging
import re
import subprocess as sp
import sys
import textwrap
from contextlib import AbstractContextManager
from pathlib import Path
from typing import Any, Callable

import archspec.cpu  # type: ignore [import-untyped]
import pytest
import yaml
from mpsd_software_manager import install, spack
from mpsd_software_manager.available import query_package_sets
from mpsd_software_manager.commands import main
from mpsd_software_manager.config import MPSD_SOFTWARE_ROOT_MARKER, Config


@pytest.fixture(scope="module")
def persistent_tmp_path(tmp_path_factory: pytest.TempPathFactory) -> Path:
    """Shared tmp_path for all tests in the module."""
    return tmp_path_factory.mktemp("workflow")


RELEASE = "develop"
TOOLCHAIN = "toolchain_example"


#################################
# Initialise MPSD software root #
#################################


def test_init(
    persistent_tmp_path: Path,
    cd: Callable[[Path], AbstractContextManager[None]],
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Initialise MPSD software root."""
    assert not (persistent_tmp_path / MPSD_SOFTWARE_ROOT_MARKER).exists()

    monkeypatch.setattr(sys, "argv", ["mpsd-software", "init"])
    with cd(persistent_tmp_path):
        main()

    assert (persistent_tmp_path / MPSD_SOFTWARE_ROOT_MARKER).exists()


#############################
# Query installation status #
#############################


@pytest.mark.order(after="test_init")
def test_status_empty(
    persistent_tmp_path: Path,
    cd: Callable[[Path], AbstractContextManager[None]],
    monkeypatch: pytest.MonkeyPatch,
    caplog: pytest.LogCaptureFixture,
) -> None:
    """Status of empty MPSD software release."""
    caplog.set_level(logging.INFO)
    monkeypatch.setattr(sys, "argv", ["mpsd-software", "status"])
    with cd(persistent_tmp_path):
        main()
    assert "No software release is installed" in caplog.text


@pytest.mark.order(after="test_status_empty")
def test_status_releases_empty(
    persistent_tmp_path: Path,
    cd: Callable[[Path], AbstractContextManager[None]],
    monkeypatch: pytest.MonkeyPatch,
    caplog: pytest.LogCaptureFixture,
) -> None:
    """Toolchain status of empty MPSD software release."""
    caplog.set_level(logging.INFO)
    monkeypatch.setattr(sys, "argv", ["mpsd-software", "status", "develop"])
    with cd(persistent_tmp_path):
        main()

    assert "No toolchains are installed" in caplog.text


@pytest.mark.xfail
@pytest.mark.order(after="test_status_releases_empty")
def test_status_package_sets_empty(
    persistent_tmp_path: Path,
    cd: Callable[[Path], AbstractContextManager[None]],
    monkeypatch: pytest.MonkeyPatch,
    caplog: pytest.LogCaptureFixture,
) -> None:
    """Querying packages in a non-existing release fails."""
    caplog.set_level(logging.INFO)
    monkeypatch.setattr(
        sys, "argv", ["mpsd-software", "status", "develop", "foss2023a"]
    )
    with cd(persistent_tmp_path):
        main()

    assert "No toolchains are installed" in caplog.text


#########################################
# Install global packages and toolchain #
#########################################


@pytest.mark.order(after="test_status_package_sets_empty")
def test_install_unknown_release(
    persistent_tmp_path: Path,
    cd: Callable[[Path], AbstractContextManager[None]],
    monkeypatch: pytest.MonkeyPatch,
    caplog: pytest.LogCaptureFixture,
) -> None:
    """Installing an unknown software release fails."""
    caplog.set_level(logging.INFO)
    monkeypatch.setattr(
        sys, "argv", ["mpsd-software", "install", "invalid_release", "global"]
    )
    with cd(persistent_tmp_path), pytest.raises(SystemExit):
        main()

    # no directories or files are created from the invalid installation request
    assert list(persistent_tmp_path.iterdir()) == [
        persistent_tmp_path / MPSD_SOFTWARE_ROOT_MARKER
    ]


@pytest.mark.order(after="test_install_unknown_release")
def test_install_unknown_package_set(
    persistent_tmp_path: Path,
    cd: Callable[[Path], AbstractContextManager[None]],
    monkeypatch: pytest.MonkeyPatch,
    caplog: pytest.LogCaptureFixture,
) -> None:
    """Installing an unknown package set fails."""
    caplog.set_level(logging.INFO)
    monkeypatch.setattr(
        sys, "argv", ["mpsd-software", "install", "develop", "invalid_software_release"]
    )
    with cd(persistent_tmp_path), pytest.raises(SystemExit):
        main()

    # no directories or files are created from the invalid installation request
    assert list(persistent_tmp_path.iterdir()) == [
        persistent_tmp_path / MPSD_SOFTWARE_ROOT_MARKER
    ]


@pytest.mark.order(after="test_install_unknown_package_set")
def test_prepare_installation(
    persistent_tmp_path: Path,
    cd: Callable[[Path], AbstractContextManager[None]],
    monkeypatch: pytest.MonkeyPatch,
    caplog: pytest.LogCaptureFixture,
) -> None:
    """
    Prepare directory structure and folders by 'installing' global package set in
    develop release. We monkeypatch spack.install_package_set to not actually install
    anything during this tests because installing a real package set is too slow. The
    subsequent tests will install minimalistic versions of a set of global packages
    and a toolchain.
    """
    caplog.set_level(logging.INFO)

    def do_nothing(*args: Any, **kwargs: Any) -> None:
        pass

    monkeypatch.setattr(install.spack, "install_package_set", do_nothing)  # type: ignore [attr-defined]
    source_cache = persistent_tmp_path / "source_cache"
    source_cache.mkdir()
    monkeypatch.setattr(
        sys,
        "argv",
        [
            "mpsd-software",
            "install",
            "--source-cache-path",
            str(source_cache),
            RELEASE,
            "global",
        ],
    )
    with cd(persistent_tmp_path):
        main()

    microarch: str = archspec.cpu.detect.host().name
    assert Config().spack_root == persistent_tmp_path / RELEASE / microarch / "spack"
    assert Config().spack_root.is_dir()
    assert (Config().spack_root / "etc" / "spack" / "modules.yaml").is_file()

    assert Config().spack_environments_root.is_dir()
    assert (Config().spack_environments_root / "toolchains").is_dir()

    assert "switching to branch" in caplog.text
    assert "detecting system compiler" in caplog.text
    assert "installing package set" in caplog.text


@pytest.mark.order(after="test_status_package_sets_empty")
def test_install_global_packages(
    persistent_tmp_path: Path,
    cd: Callable[[Path], AbstractContextManager[None]],
    monkeypatch: pytest.MonkeyPatch,
    caplog: pytest.LogCaptureFixture,
) -> None:
    """Install global packages.

    We create a list of packages that are quick to install. This package set is however
    not available in the spack-environments repository. Therefore, we have to patch
    the query of available packages.
    """
    caplog.set_level(logging.INFO)
    package_set = "global_example"
    toolchain_dir = (
        persistent_tmp_path
        / RELEASE
        / "spack-environments"
        / "toolchains"
        / package_set
    )
    toolchain_dir.mkdir()

    with (toolchain_dir / "global_packages.list").open("w") as f:
        f.write("gmake\n")

    source_cache = persistent_tmp_path / "source_cache"
    monkeypatch.setattr(
        sys,
        "argv",
        [
            "mpsd-software",
            "install",
            "--source-cache-path",
            str(source_cache),
            RELEASE,
            package_set,
        ],
    )

    # overwrite modules.yaml settings to not blacklist gmake in order to test the
    # module file generation
    modules_yaml_file = (
        persistent_tmp_path
        / RELEASE
        / "spack-environments"
        / "spack_overlay"
        / "etc"
        / "spack"
        / "modules.yaml.jinja"
    )
    with modules_yaml_file.open() as f:
        modules_yaml = yaml.load(f, Loader=yaml.Loader)

    # zlib is required for the next test; for convenience set it here
    modules_yaml["modules"]["default"]["lmod"]["include"].extend(
        [
            "gmake%{{ system_compiler }}",
            "zlib%{{ system_compiler }}",
        ]
    )

    with modules_yaml_file.open("w") as f:
        yaml.dump(modules_yaml, stream=f, Dumper=yaml.Dumper)

    # commit changes, otherwise git rebase call will fail
    sp.check_call(
        "git commit -am 'Update modules.yaml'",
        shell=True,
        cwd=persistent_tmp_path / RELEASE / "spack-environments",
    )

    def extendent_query_package_sets(software_release: str) -> list[str]:
        return query_package_sets(software_release) + [package_set]

    # The global_examples package set does not exist in spack-environments so
    # the installation would fail because we request an invalid package set.
    monkeypatch.setattr(install, "query_package_sets", extendent_query_package_sets)

    with cd(persistent_tmp_path):
        main()

    assert "==> Installing gmake" in caplog.text
    assert "refreshing lmod modules" in caplog.text
    assert "generating lmod metamodules" not in caplog.text
    assert f"{package_set}: Installation succeeded" in caplog.text

    assert (Config().source_cache_root / "gmake").is_dir()

    assert (Config().lmod_root / "Core" / "gmake").exists()


@pytest.mark.order(after="test_install_global_packages")
def test_install_toolchain(
    persistent_tmp_path: Path,
    cd: Callable[[Path], AbstractContextManager[None]],
    monkeypatch: pytest.MonkeyPatch,
    caplog: pytest.LogCaptureFixture,
) -> None:
    """Install toolchain package set.

    We create a toolchain with packages that are quick to install. This toolchain is
    however not available in the spack-environments repository. Therefore, we have to
    patch the query of available packages.

    We have to further patch two other functions to avoid installing a compiler, which
    would make the test too slow.

    """
    caplog.set_level(logging.INFO)
    toolchain_dir = (
        persistent_tmp_path / RELEASE / "spack-environments" / "toolchains" / TOOLCHAIN
    )
    toolchain_dir.mkdir()

    # find system compiler
    with cd(persistent_tmp_path):
        Config(persistent_tmp_path)
        Config().resolve_paths(RELEASE)
        spack.find_system_compiler(None)

    with (toolchain_dir / "compiler_vars.sh").open("w") as f:
        f.write(
            f'toolchain_compiler_package="{Config().system_compiler}"\n'
            "export TOOLCHAIN_COMPILER=$toolchain_compiler_package"
        )

    (Config().lmod_root / Config().system_compiler.replace("@", "/")).mkdir(
        parents=True
    )

    Config._instance = None

    with (toolchain_dir / "spack.yaml.jinja").open("w") as f:
        f.write(
            textwrap.dedent(
                """
                spack:
                  definitions:
                  - compilers:
                    - "{{ toolchain_compiler }}"
                  - packages:
                    - zlib
                  specs:
                  - matrix:
                    - [$packages]
                    - [$%compilers]
                  view: true
                  concretizer:
                    unify: true
                    reuse: false
                  packages:
                    all:
                      require: "%{{ toolchain_compiler }}"
                """
            )
        )

    # The TOOLCHAIN package set does not exist in spack-environments so
    # the installation would fail because we request an invalid package set.

    def extendent_query_package_sets(software_release: str) -> list[str]:
        return query_package_sets(software_release) + [TOOLCHAIN]

    monkeypatch.setattr(install, "query_package_sets", extendent_query_package_sets)

    # The toolchain compiler installation function would install
    # system_compiler%system_compiler To avoid that we patch the spack_install_package
    # method to do nothing (just print in case of errors). When installing a toolchain
    # no other function calls spack_install_package

    def spack_install_package(package: str) -> None:
        print("Would now install", package)

    monkeypatch.setattr(spack, "spack_install_package", spack_install_package)

    old_spack_function = spack.spack

    def patched_spack(
        command: str, *args: Any, **kwargs: Any
    ) -> sp.CompletedProcess[str]:
        if command.startswith("location -i gcc"):
            return sp.CompletedProcess(command, returncode=0, stdout="/usr/bin")
        else:
            return old_spack_function(command, *args, **kwargs)

    monkeypatch.setattr(spack, "spack", patched_spack)

    source_cache = persistent_tmp_path / "source_cache"
    monkeypatch.setattr(
        sys,
        "argv",
        [
            "mpsd-software",
            "install",
            "--source-cache-path",
            str(source_cache),
            RELEASE,
            TOOLCHAIN,
        ],
    )
    with cd(persistent_tmp_path / RELEASE):  # the calling directory does not matter
        main()

    assert "==> Installing zlib" in caplog.text
    assert "refreshing lmod modules" in caplog.text
    assert "generating lmod metamodules" in caplog.text
    assert f"{TOOLCHAIN}: Installation succeeded" in caplog.text

    # not "toolchain" metamodules for new releases
    toolchain_module = Config().lmod_root / "Core" / "toolchain" / f"{TOOLCHAIN}.lua"
    assert not toolchain_module.is_file()

    octopus_full_dependencies_module = (
        Config().lmod_root
        / Config().system_compiler.replace("@", "/")
        / "octopus-dependencies"
        / "full.lua"
    )
    assert octopus_full_dependencies_module.is_file()
    with octopus_full_dependencies_module.open() as f:
        octopus_full_dependencies_content = f.read()
    assert 'depends_on("zlib' in octopus_full_dependencies_content

    octopus_min_dependencies_module = (
        Config().lmod_root
        / Config().system_compiler.replace("@", "/")
        / "octopus-dependencies"
        / "min.lua"
    )
    assert octopus_min_dependencies_module.is_file()
    with octopus_min_dependencies_module.open() as f:
        octopus_min_dependencies_content = f.read()
    # in this test environment we do not install any package that is part of the
    # octopus-dependencies/min metamodule
    assert "depends_on" not in octopus_min_dependencies_content


#############################
# Query installation status #
#############################


@pytest.mark.order(after="test_install_toolchain")
def test_status_installed(
    persistent_tmp_path: Path,
    cd: Callable[[Path], AbstractContextManager[None]],
    monkeypatch: pytest.MonkeyPatch,
    caplog: pytest.LogCaptureFixture,
) -> None:
    """Test query of overview of installed software releases."""
    caplog.set_level(logging.INFO)
    monkeypatch.setattr(sys, "argv", ["mpsd-software", "status"])
    with cd(persistent_tmp_path):
        main()
    assert "Installed MPSD software releases:" in caplog.text
    assert RELEASE in caplog.text


@pytest.mark.xfail(reason="Status command relies on deprecated toolchain metamodule")
@pytest.mark.order(after="test_install_toolchain")
def test_status_releases_installed(
    persistent_tmp_path: Path,
    cd: Callable[[Path], AbstractContextManager[None]],
    monkeypatch: pytest.MonkeyPatch,
    caplog: pytest.LogCaptureFixture,
) -> None:
    """Test query of overview of installed package sets."""
    caplog.set_level(logging.INFO)
    monkeypatch.setattr(sys, "argv", ["mpsd-software", "status", RELEASE])
    with cd(persistent_tmp_path):
        main()

    assert TOOLCHAIN in caplog.text
    # global packages are not shown in status
    assert "global_example" not in caplog.text


@pytest.mark.order(after="test_install_toolchain")
def test_status_package_sets_installed(
    persistent_tmp_path: Path,
    cd: Callable[[Path], AbstractContextManager[None]],
    monkeypatch: pytest.MonkeyPatch,
    caplog: pytest.LogCaptureFixture,
) -> None:
    """Test query of overview of installed packages."""
    caplog.set_level(logging.INFO)
    monkeypatch.setattr(sys, "argv", ["mpsd-software", "status", RELEASE, TOOLCHAIN])
    with cd(persistent_tmp_path):
        main()

    assert f"==> In environment {TOOLCHAIN}" in caplog.text
    assert "[+] zlib" in caplog.text


@pytest.mark.order(after="test_install_toolchain")
def test_status_package_sets_installed_detailed(
    persistent_tmp_path: Path,
    cd: Callable[[Path], AbstractContextManager[None]],
    monkeypatch: pytest.MonkeyPatch,
    caplog: pytest.LogCaptureFixture,
) -> None:
    """Test query of details of installed packages."""
    caplog.set_level(logging.INFO)
    monkeypatch.setattr(
        sys,
        "argv",
        ["mpsd-software", "status", "--package-details", RELEASE, TOOLCHAIN],
    )
    with cd(persistent_tmp_path):
        main()

    assert re.search(r"zlib@=[0-9.]+%gcc@=[0-9.]+.*arch=", caplog.text) is not None
