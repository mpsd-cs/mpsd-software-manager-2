"""Test individual functions in spack wrapper mocking the spack calls."""

import subprocess as sp
import sys
import textwrap
from pathlib import Path
from typing import Any

if sys.version_info.minor < 11:
    from typing_extensions import Self
else:
    from typing import Self

import pytest
from mpsd_software_manager import spack

compiler_template = """
{compiler}
    paths:
        cc = {path}gcc
        cxx = {path}g++
        f77 = {path}gfortran
        fc = {path}gfortran
    modules = []
"""


class MockConfig:
    """Minimal Config object with only the rquired properties."""

    system_compiler = None

    @property
    def spack_root(self) -> Path:
        """Path to fake spack root."""
        return Path("/path/to/spack")

    def __call__(self) -> Self:
        """
        Call implemented to fake the singleton behaviour and use the same config object
        in the test functions and inside the `find_system_compiler` function.
        """
        return self


def test_no_compiler(monkeypatch: pytest.MonkeyPatch) -> None:
    """
    Picking or autoselecting a system compiler does not work if no compilers are known
    to spack.
    """
    monkeypatch.setattr(spack, "Config", MockConfig())

    # no compiler known to spack
    available_compilers = textwrap.dedent(
        """
        ==> No compilers available. Run `spack compiler find` to autodetect compilers
        """
    )

    def fake_spack(command: str, *args: Any, **kwargs: Any) -> sp.CompletedProcess[str]:
        # unused *args, **kwargs to ignore logging requests

        # available_compilers and compiler_info will be re-defined multiple times
        # for the different test cases
        if command.startswith("compiler find"):
            return sp.CompletedProcess(
                command,
                returncode=0,
            )
        elif command.startswith("compiler list"):
            return sp.CompletedProcess(
                command, returncode=0, stdout=available_compilers
            )
        else:
            raise RuntimeError(f"Unknown command {command}")

    monkeypatch.setattr(spack, "spack", fake_spack)

    with pytest.raises(SystemExit):
        spack.find_system_compiler(None)

    with pytest.raises(SystemExit):
        spack.find_system_compiler("gcc@12.2.0")


def test_one_system_compiler(monkeypatch: pytest.MonkeyPatch) -> None:
    """
    Autodetection and manual selection are supported when one system compiler is
    available.
    """
    mock_config = MockConfig()
    monkeypatch.setattr(spack, "Config", mock_config)

    available_compilers = textwrap.dedent(
        """
        ==> Available compilers
        -- gcc ---
        gcc@12.2.0
        gcc@13.2.0
        """
    )

    def compiler_info(command: str) -> str:
        if "gcc@12.2.0" in command:
            return compiler_template.format(compiler="gcc@12.2.0", path="/usr/bin/")
        elif "gcc@13.2.0" in command:
            return compiler_template.format(
                compiler="gcc@13.2.0", path="/path/to/spack/"
            )
        else:
            raise RuntimeError(f"invalid command {command}")

    def fake_spack(command: str, *args: Any, **kwargs: Any) -> sp.CompletedProcess[str]:
        # unused *args, **kwargs to ignore logging requests

        # available_compilers and compiler_info will be re-defined multiple times
        # for the different test cases
        if command.startswith("compiler find"):
            return sp.CompletedProcess(
                command,
                returncode=0,
            )
        elif command.startswith("compiler list"):
            return sp.CompletedProcess(
                command, returncode=0, stdout=available_compilers
            )
        else:
            stdout = compiler_info(command)
            return sp.CompletedProcess(command, returncode=0, stdout=stdout)

    monkeypatch.setattr(spack, "spack", fake_spack)

    # requesting an unknown compiler fails
    with pytest.raises(SystemExit):
        spack.find_system_compiler("gcc@14.1.0")
    assert mock_config.system_compiler is None

    # auto-detection works
    spack.find_system_compiler(None)
    assert mock_config.system_compiler == "gcc@12.2.0"

    mock_config = MockConfig()
    monkeypatch.setattr(spack, "Config", mock_config)

    # requesting a known compiler works
    spack.find_system_compiler("gcc@12.2.0")
    assert mock_config.system_compiler == "gcc@12.2.0"

    # requesting a spack compiler as system compiler does not work
    with pytest.raises(SystemExit):
        spack.find_system_compiler("gcc@13.2.0")
    assert mock_config.system_compiler == "gcc@12.2.0"


def test_multiple_system_compilers(monkeypatch: pytest.MonkeyPatch) -> None:
    """
    Autodetection does not work if multiple system compilers are available. Manual
    selection of any of the system compilers works.
    """
    mock_config = MockConfig()
    monkeypatch.setattr(spack, "Config", mock_config)

    available_compilers = textwrap.dedent(
        """
        ==> Available compilers
        -- gcc ---
        gcc@12.2.0
        gcc@13.2.0
        """
    )

    def compiler_info(command: str) -> str:
        if "gcc@12.2.0" in command:
            return compiler_template.format(compiler="gcc@12.2.0", path="/usr/bin/")
        elif "gcc@13.2.0" in command:
            return compiler_template.format(compiler="gcc@13.2.0", path="/usr/bin/")
        else:
            raise RuntimeError(f"invalid command {command}")

    def fake_spack(command: str, *args: Any, **kwargs: Any) -> sp.CompletedProcess[str]:
        # unused *args, **kwargs to ignore logging requests

        # available_compilers and compiler_info will be re-defined multiple times
        # for the different test cases
        if command.startswith("compiler find"):
            return sp.CompletedProcess(
                command,
                returncode=0,
            )
        elif command.startswith("compiler list"):
            return sp.CompletedProcess(
                command, returncode=0, stdout=available_compilers
            )
        else:
            stdout = compiler_info(command)
            return sp.CompletedProcess(command, returncode=0, stdout=stdout)

    monkeypatch.setattr(spack, "spack", fake_spack)

    # auto-detection does not work
    with pytest.raises(SystemExit):
        spack.find_system_compiler(None)
    assert mock_config.system_compiler is None

    # selecting an unknown compiler fails
    with pytest.raises(SystemExit):
        spack.find_system_compiler("gcc@14.1.0")
    assert mock_config.system_compiler is None

    # requesting a known compiler works
    spack.find_system_compiler("gcc@12.2.0")
    assert mock_config.system_compiler == "gcc@12.2.0"

    spack.find_system_compiler("gcc@13.2.0")
    assert mock_config.system_compiler == "gcc@13.2.0"
