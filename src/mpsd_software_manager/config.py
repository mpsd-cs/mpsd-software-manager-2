"""Global configuration; can be used in each module.

Configuration is implemented as a singleton, modules should use:

.. code:: python

   from mpsd_software_manager import Config
   ...
   Config().<property>

The config object should be initialised in the command line module.
"""

from __future__ import annotations

from dataclasses import InitVar, dataclass, field
from logging import getLogger
from pathlib import Path
from typing import Any

from .util import abort, detect_cpu_microarch

MPSD_SOFTWARE_ROOT_MARKER: str = ".mpsd-software-root"

logger = getLogger(__name__)


class Singleton(type):
    """Singleton metaclass (not safe for subclasses)."""

    _instance = None

    def __call__(cls, *args: Any, **kwargs: Any) -> Any:
        """Return existing instance of cls or create new one when called first."""
        if cls._instance is None:
            cls._instance = super().__call__(*args, **kwargs)
        return cls._instance

    @property
    def initialised(cls) -> bool:
        """Return true if the singleton object has not been initialised yet."""
        return cls._instance is not None


@dataclass
class Config(metaclass=Singleton):
    """Global configuration."""

    cwd: InitVar[Path] = None

    mpsd_software_root: Path | None = field(init=False)
    microarch: str = field(init=False)
    generic_microarch: str = field(init=False)

    release_root: Path = field(init=False)
    microarch_root: Path = field(init=False)
    spack_root: Path = field(init=False)
    lmod_root: Path = field(init=False)
    spack_environments_root: Path = field(init=False)
    log_root: Path = field(init=False)

    source_cache_root: Path = Path("/opt_mpsd/mpsd_spack_sources")
    binary_cache_root: Path = field(init=False)
    spack_user_cache_root: Path = field(init=False)

    system_compiler: str = field(init=False)

    spack_environments_repository = ("gitlab.gwdg.de", "mpsd-cs/spack-environments")
    spack_repository = ("github.com", "mpsd-computational-science/spack")

    def __post_init__(self, cwd: Path) -> None:
        """Initialise mpsd_software_root and microarch information.

        Tries to find a .mpsd-software-root file in the current working directory `cwd`
        or any parent directory. If not found, mpsd_software_root is set to None.

        """
        self.mpsd_software_root = find_mpsd_software_root(cwd)
        self.generic_microarch, self.microarch = detect_cpu_microarch()

    def resolve_paths(self, mpsd_software_release: str) -> None:
        """Resolve all paths using the user-defined mpsd_software_release."""
        assert self.mpsd_software_root is not None
        self.release_root = self.mpsd_software_root / mpsd_software_release
        self.microarch_root = self.release_root / self.microarch
        self.spack_root = self.microarch_root / "spack"
        self.lmod_root = self.microarch_root / "lmod"
        self.spack_environments_root = self.release_root / "spack-environments"
        self.log_root = self.release_root / "logs"
        self.binary_cache_root = self.microarch_root / "local-spack-cache"
        self.spack_user_cache_root = self.spack_root.parent / "local-spack-cache"


def find_mpsd_software_root(cwd: Path) -> Path | None:
    """Find mpsd_software_root directory.

    The directory is marked with MPSD_SOFTWARE_ROOT_MARKER. If not directory contains
    the file, return None.
    """
    assert cwd.is_absolute()
    if (cwd / MPSD_SOFTWARE_ROOT_MARKER).exists():
        return cwd
    for parent in cwd.parents:
        if (parent / MPSD_SOFTWARE_ROOT_MARKER).exists():
            return parent
    return None


def require_valid_software_root(command: str) -> None:
    """Terminate program if mpsd_software_root is None."""
    if Config().mpsd_software_root is None:
        abort(f"Call to '{command}' outside of MPSD software directory tree.")
