"""Handler for logging to stdout and a file."""

from __future__ import annotations

import datetime
import logging

from rich.logging import RichHandler

from .config import Config

logger = logging.getLogger(__name__)


def init_stdout_logger(verbose: bool = False) -> None:
    """Show log messages on stdout."""
    level = logging.DEBUG if verbose else logging.INFO
    logging.basicConfig(
        format="%(message)s",
        handlers=[RichHandler(level=level, show_time=False, show_path=False)],
    )
    # set only interal logging to a low level, ignore third party
    logging.getLogger("mpsd_software_manager").setLevel(level)


def init_file_logger() -> None:
    """Write log messages to a file."""
    config = Config()
    file_name = (
        f"{config.microarch}-{datetime.datetime.now().isoformat(timespec='seconds')}"
    )
    config.log_root.mkdir(parents=True, exist_ok=True)
    log_file = config.log_root / file_name
    if log_file.exists():
        logger.error("Log file '%s' exists already; skipping file logging", log_file)
        return
    else:
        logger.info("Detailed log written to '%s'", log_file)
    logging.getLogger("mpsd_software_manager").setLevel(logging.DEBUG)
    logging.getLogger().addHandler(logging.FileHandler(filename=log_file))
