"""Helper functions."""

from __future__ import annotations

import inspect
import sys
from logging import getLogger
from pathlib import Path

import archspec.cpu  # type: ignore

logger = getLogger(__name__)


def abort(message: str, exit_code: int = 1) -> None:
    """Show error message and exit."""
    calling_frame = inspect.currentframe().f_back  # type: ignore [union-attr]
    called_from = inspect.getmodule(calling_frame).__name__  # type: ignore [union-attr]
    getLogger(called_from).error(message)
    sys.exit(exit_code)


def detect_cpu_microarch() -> tuple[str, str]:
    """Detect architecture and microarch of current host using archspec.

    Generic microarch defaults to 'sandybridge'. When running on 'westmere' it is set to
    'westmere'; when running on 'power8le' it is set to 'power8le'. Other
    (micro-)architectures than x86-64 or older than sandybridge (avx support) are not
    supported.

    """
    microarch: str = archspec.cpu.detect.host().name

    if microarch == "power8le":
        generic_microarch = "power8le"
    elif microarch == "westmere":
        generic_microarch = "westmere"
    else:
        generic_microarch = "sandybridge"

    return generic_microarch, microarch


def initialise_dir(directory: Path, directory_type: str) -> None:
    """Initialise 'directory' if it does not exist; terminate program on error."""
    if not directory.exists():
        directory.mkdir()
        logger.info("Creating new %s directory '%s'", directory_type, directory)
    elif not directory.is_dir():
        abort(f"'{directory}' exists but is not a directory.")
    else:
        logger.info("Using existing %s directory '%s'", directory_type, directory)
