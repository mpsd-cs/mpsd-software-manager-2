# MPSD software manager

This repository provides the `mpsd-software` tool which is used to install
package sets and toolchains on the
[MPSD HPC cluster](https://computational-science.mpsd.mpg.de/docs/mpsd-hpc.html).

It can also be used to install the software on other machines, such as Linux
laptops and desktops. This can be useful to work - on a local machine - with
the same software environment, for example to debug a problem.

## Quick start

```shell
pipx install git+https://gitlab.gwdg.de/mpsd-cs/mpsd-software-manager-2
cd /path/to/desired/mpsd-software-root
mpsd-software init
mpsd-software -h
mpsd-software available [...]  # see mpsd-software available --help
mpsd-software install develop-oneapi foss2023a-serial global
mpsd-software status [...]  # see mpsd-software status --help
```

## Version compatibility

We use semantic versioning, major versions for relevant (backwards-incomatible)
new/changed features in Spack. The following table provides an overview over
available `mpsd-software` versions and compatible MPSD software releases (and
corresponding Spack releases).

| MPSD software releases (spack version) | `mpsd-software` |
|----------------------------------------|-----------------|
| develop-oneapi (develop, planned: v0.23) | 1.x |
| dev-23a (v0.19), 23b (v0.19), 24a (v0.21) | not compatible (use the [old software manager](https://gitlab.gwdg.de/mpsd-cs/mpsd-software-manager)) |

## Concepts and terminology

[Currently explained here](https://gitlab.gwdg.de/mpsd-cs/mpsd-software-manager)
