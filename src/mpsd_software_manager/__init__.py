"""Command-line tool to install software using Spack."""

import sys
from logging import getLogger
from typing import Any

from ._version import __version__ as __version__

logger = getLogger(__name__)


def silent_keyboard_interrupt(
    type_: type[BaseException], value: BaseException, traceback: Any
) -> None:
    """Do not show stacktrace on keyboard interrupt."""
    if type_ is KeyboardInterrupt:
        sys.stderr.write("\n")
        logger.error("Keyboard interrupt")
    else:
        sys.__excepthook__(type_, value, traceback)


sys.excepthook = silent_keyboard_interrupt
