"""Query status of installed software."""

from __future__ import annotations

import argparse
import subprocess as sp
import sys
from logging import getLogger

from .config import Config, require_valid_software_root
from .spack import spack
from .util import abort

logger = getLogger(__name__)


def status(args: argparse.Namespace) -> None:
    """Status of installed releases and package sets."""
    require_valid_software_root(command="status")

    if args.software_release is None:
        mpsd_software_status()
    elif args.package_set is None:
        release_status(args.software_release)
    else:
        package_set_status(
            args.software_release, args.package_set, args.package_details
        )


def mpsd_software_status() -> None:
    """Show summary information about MPSD software installation."""
    c = Config()
    assert c.mpsd_software_root is not None
    software_releases = ", ".join(
        sorted(path.name for path in c.mpsd_software_root.iterdir() if path.is_dir())
    )

    logger.info("MPSD software root: '%s'", c.mpsd_software_root)
    logger.info("Installed MPSD software releases:")
    logger.info(software_releases or "<No software release is installed>")


def release_status(software_release: str) -> None:
    """Show information about software release.

    A list of installed package_sets per microarch is displayed.
    """
    c = Config()
    c.resolve_paths(software_release)

    logger.info("MPSD software root: '%s'", c.mpsd_software_root)
    if not c.release_root.is_dir():
        logger.info("No toolchains are installed.")
        return

    microarch_dirs = sorted(path for path in c.release_root.iterdir() if path.is_dir())
    logger.info("Available toolchains in software release '%s'", software_release)
    for microarch_dir in microarch_dirs:
        if microarch_dir.name in ["logs", "spack-environments"]:
            continue
        logger.info("%s:", microarch_dir.name)
        lmod_toolchain_dir = microarch_dir / "lmod" / "Core" / "toolchain"
        if not lmod_toolchain_dir.is_dir():
            logger.info("<No toolchains are fully installed.>")
            continue
        toolchains = sorted(path.stem for path in lmod_toolchain_dir.glob("*.lua"))
        logger.info(", ".join(toolchains))


def package_set_status(
    software_release: str, package_set: str, package_details: bool
) -> None:
    """Show information about 'package_set' of 'software_release'.

    Only the current microarch is considered. Formatting is based on spack.
    """
    Config().resolve_paths(software_release)
    if "global" in package_set:
        abort("Querying global package sets is not yet supported.")
    if package_details:
        find_options = (
            "--format {name}{@versions}"
            "{%compiler.name}{@compiler.versions}{compiler_flags}"
            "{variants}{arch=architecture}"
        )
    else:
        find_options = "-r"

    try:
        packages = spack(f"-e {package_set} find {find_options}").stdout
    except sp.CalledProcessError:
        sys.exit(1)
    for line in packages.split("\n"):
        if line == "":
            continue
        logger.info(line)
