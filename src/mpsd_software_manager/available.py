"""Query releases and package sets available for installation."""

from __future__ import annotations

import argparse
import functools
from logging import getLogger

import gitlab

from ._version import __version__
from .config import Config
from .release_compatibility import compatible_releases
from .util import abort

logger = getLogger(__name__)


def available(args: argparse.Namespace) -> None:
    """List available software releases or package sets."""
    if args.software_release is None:
        available_releases()
    else:
        available_package_sets(args.software_release)


def available_releases() -> None:
    """List available MPSD software releases.

    Releases are hard-coded in the function.
    """
    releases = set(query_release_branches() + ["develop"])
    compatible = compatible_releases[__version__.split(".")[0]]
    logger.info("Compatible MPSD software releases:")
    logger.info(", ".join(sorted(releases & compatible)))
    logger.info("[Other releases, incompatible with mpsd-software %s]", __version__)
    logger.info("[%s]", ", ".join(sorted(releases - compatible)))


def available_package_sets(software_release: str) -> None:
    """List available package sets for a given release."""
    package_sets = query_package_sets(software_release)
    logger.info("Available package sets for release '%s':", software_release)
    for package_set in package_sets:
        logger.info("  - %s", package_set)


@functools.cache
def query_package_sets(software_release: str) -> list[str]:
    """Query available package sets for a given MPSD software release."""
    if software_release in query_release_branches():
        branch = f"releases/{software_release}"
    else:
        try:
            spack_environments_repository().branches.get(software_release)
        except gitlab.GitlabGetError:
            abort(f"'{software_release}' is not a valid MPSD software release")
        else:
            branch = software_release

    package_sets_tree = spack_environments_repository().repository_tree(
        path="toolchains", ref=branch, get_all=True
    )
    return [elem["name"] for elem in package_sets_tree]


@functools.cache
def query_release_branches() -> list[str]:
    """Get list of released branches.

    The 'releases/' prefix is stripped off.
    """
    branches = spack_environments_repository().branches.list(get_all=True)
    releases = [
        branch.name.removeprefix("releases/")
        for branch in branches
        if branch.name.startswith("releases/")
    ]
    return releases


@functools.cache
def spack_environments_repository() -> gitlab.v4.objects.projects.Project:
    """Object to query the spack-environments repository."""
    repo = Config().spack_environments_repository
    gl = gitlab.Gitlab(f"https://{repo[0]}")
    return gl.projects.get(repo[1])
