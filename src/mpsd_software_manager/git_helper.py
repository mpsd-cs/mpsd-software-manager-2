"""Helper module to simplify required git operations."""

from __future__ import annotations

import subprocess as sp
from logging import getLogger
from pathlib import Path

from .util import abort

logger = getLogger(__name__)


class Git:
    """Perform selected git operations for a given 'target' directory."""

    def __init__(self, target: Path) -> None:
        self._target = target

    def clone(self, repository: tuple[str, str], protocol: str) -> None:
        """Clone 'repository' using the specified 'protocol' (ssh or https)."""
        if protocol == "https":
            url = f"https://{repository[0]}/{repository[1]}.git"
        elif protocol == "ssh":
            url = f"git@{repository[0]}:{repository[1]}.git"
        else:
            abort(f"Protocol '{protocol}' is not supported for 'git clone'.")

        logger.info("git: cloning '%s' into '%s'", url, self._target)
        sp.run(
            ["git", "clone", url, str(self._target)],
            cwd=self._target.parent,
            capture_output=True,
            check=True,
        )

    def fetch(self) -> None:
        """Run 'git fetch' in target."""
        logger.info("git: fetching changes in '%s'", self._target)
        sp.run(["git", "fetch"], cwd=self._target, capture_output=True, check=True)

    def checkout(self, branch: str) -> None:
        """Run 'git checkout branch' in target."""
        logger.info("git: switching to branch '%s'", branch)
        sp.run(
            ["git", "checkout", branch],
            cwd=self._target,
            capture_output=True,
            check=True,
        )

    def pull(self) -> None:
        """Run 'git pull' in target."""
        logger.info("git: pulling latest changes")
        sp.run(
            ["git", "pull", "--rebase"],
            cwd=self._target,
            capture_output=True,
            check=True,
        )


def initialise_or_update_repository(
    repository: tuple[str, str], target: Path, git_protocol: str, branch: str
) -> None:
    """Clone or fetch 'repository' to 'target' and switch to 'branch'."""
    git = Git(target=target)

    if not target.is_dir():
        git.clone(repository, git_protocol)
    else:
        git.fetch()

    git.checkout(branch)
    git.pull()
