1.0.dev1
========

- Switch to Jinja templating
- Re-write of the metamodule generation:
  - Toolchain metamodules are deprecated and will only be generated for the existing toolchains
  - The octopus-dependencies module does now come in two variants min and full


1.0.dev0
========

- Re-write of the old ``spack_setup.sh`` and ``software-manager``.
