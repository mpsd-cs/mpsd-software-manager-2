"""Initialise mpsd_software_root on disk."""

from __future__ import annotations

import argparse
import datetime
from logging import getLogger
from pathlib import Path

import mpsd_software_manager as smgr

from .config import MPSD_SOFTWARE_ROOT_MARKER

logger = getLogger(__name__)


def init(_args: argparse.Namespace) -> None:
    """Initialise directory as mpsd software root.

    - create .mpsd-software-root file as a marker
    - initialise logging

    If the current directory is already initialised no action will be performed.
    """
    cwd = Path.cwd()

    if (cwd / MPSD_SOFTWARE_ROOT_MARKER).exists():
        logger.info(
            "Directory '%s' is already initialised as MPSD software root;"
            " no action is performed.",
            cwd,
        )
        return

    now = datetime.datetime.now().isoformat(timespec="seconds")
    with (cwd / MPSD_SOFTWARE_ROOT_MARKER).open("w", encoding="utf-8") as f:
        f.write(
            f"Directory initialised as MPSD software root on {now} using MPSD software"
            f"manager version {smgr.__version__}.\n"
        )
    logger.info("Directory '%s' initialised as MPSD software root.", cwd)
