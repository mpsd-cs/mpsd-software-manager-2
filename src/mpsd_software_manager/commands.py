"""Command line entry module."""

from __future__ import annotations

import argparse
from logging import getLogger
from pathlib import Path

import mpsd_software_manager as smgr

from .available import available
from .config import Config
from .init import init
from .install import install
from .log_handler import init_stdout_logger
from .status import status

logger = getLogger(__name__)


def initialise_parser() -> argparse.ArgumentParser:
    """Create argument parser."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--verbose", action="store_true", help="verbose output on stdout"
    )
    parser.add_argument("--version", action="version", version=smgr.__version__)

    subparsers = parser.add_subparsers(required=True)

    cmd_init = subparsers.add_parser(
        "init", help="initialise a directory as mpsd software root"
    )
    cmd_init.set_defaults(func=init)

    cmd_available = subparsers.add_parser(
        "available", help="Show packages available for installation"
    )
    cmd_available.add_argument(
        "software_release", help="MPSD software release", nargs="?"
    )
    cmd_available.set_defaults(func=available)

    cmd_install = subparsers.add_parser("install", help="Install package sets")
    cmd_install.add_argument("software_release", help="MPSD software release")
    cmd_install.add_argument("package_set", help="Package set or toolchain", nargs="+")
    cmd_install.add_argument(
        "--fail-fast",
        action="store_true",
        help="Fail after first installation error (otherwise installation will be"
        " attempted for all remaining package sets)",
    )
    cmd_install.add_argument(
        "--source-cache-path",
        required=False,
        default="/opt_mpsd/mpsd_spack_sources",
        help="Path to the source cache",
        type=Path,
    )
    cmd_install.add_argument(
        "--disable-binary-cache",
        action="store_true",
        help="Do not create a local binary cache",
    )
    cmd_install.add_argument(
        "--git-protocol",
        choices=["https", "ssh"],
        default="https",
        help="Protocol used for the git clone of spack [GitHub] and"
        " spack_environments [GWDG]",
    )
    cmd_install.add_argument(
        "--no-pull",
        action="store_true",
        help="Do not fetch changes in Spack and spack environments repositories",
    )
    cmd_install.add_argument(
        "--install-dir",
        help="Directory to install to (replaces branch name); for development only",
    )
    cmd_install.add_argument("--spack-verbosity", required=False)
    cmd_install.add_argument("--system-compiler", required=False)
    cmd_install.set_defaults(func=install)

    cmd_status = subparsers.add_parser("status", help="Show status of installation")
    cmd_status.add_argument("software_release", help="MPSD software release", nargs="?")
    cmd_status.add_argument("package_set", help="Package set or toolchain", nargs="?")
    cmd_status.add_argument(
        "--package-details",
        action="store_true",
        help=(
            "Include package variants and implicit dependencies.\n"
            "NOTE: There is no indication for missing package when using this option."
        ),
    )
    cmd_status.set_defaults(func=status)

    return parser


def main() -> None:
    """Initialise argparser and call correct function."""
    parser = initialise_parser()
    args = parser.parse_args()

    init_stdout_logger(args.verbose)
    assert not Config.initialised
    Config(Path.cwd())

    args.func(args)
