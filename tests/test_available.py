"""Test queries to spack-environments repository."""

from __future__ import annotations

import logging

import pytest
from mpsd_software_manager import __version__
from mpsd_software_manager.available import available
from mpsd_software_manager.commands import initialise_parser
from mpsd_software_manager.release_compatibility import compatible_releases


@pytest.mark.default_config
def test_available_releases(caplog: pytest.LogCaptureFixture) -> None:
    """Test that a few known releases are in the list of releases."""
    caplog.set_level(logging.INFO)
    caplog.clear()
    available(initialise_parser().parse_args(["available"]))
    known_available_releases = ["dev-23a", "24a"]
    for release in known_available_releases:
        assert release in caplog.text
    # compatible_releases provides a set of releases that are compatible
    # with the current major version of mpsd_software_manager
    for release in compatible_releases[__version__.split(".")[0]]:
        assert release in caplog.text


@pytest.mark.default_config
def test_available_package_sets(caplog: pytest.LogCaptureFixture) -> None:
    """Test that a few known package sets are available in 24a MPSD software release."""
    package_set_examples = [
        "foss2022a-cuda-mpi",
        "foss2023a-serial",
        "intel2023a-mpi",
        "global",
        "global_generic",
    ]
    caplog.set_level(logging.INFO)
    caplog.clear()
    available(initialise_parser().parse_args(["available", "24a"]))
    assert "Available package sets for release '24a'" in caplog.text
    for package_set in package_set_examples:
        assert package_set in caplog.text


@pytest.mark.default_config
def test_available_package_sets_develop(caplog: pytest.LogCaptureFixture) -> None:
    """Test that the global package set is available in develop MPSD software release.

    Other package sets cannot be queried because their versions will change over time.
    """
    caplog.set_level(logging.INFO)
    caplog.clear()
    available(initialise_parser().parse_args(["available", "develop"]))
    assert "Available package sets for release 'develop'" in caplog.text
    assert "global" in caplog.text


@pytest.mark.default_config
def test_available_package_sets_wrong_branch(caplog: pytest.LogCaptureFixture) -> None:
    """Querying available package sets for a wrong branch fails and shows an error."""
    caplog.set_level(logging.ERROR)
    with pytest.raises(SystemExit):
        available(initialise_parser().parse_args(["available", "nonexisting_release"]))
    assert "'nonexisting_release' is not a valid MPSD software release" in caplog.text
