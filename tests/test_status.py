"""Test mpsd-software status with mocked directories/files."""

from __future__ import annotations

import logging
import subprocess as sp
from contextlib import AbstractContextManager
from pathlib import Path
from typing import Callable

import pytest
from mpsd_software_manager import status as status_module
from mpsd_software_manager.commands import initialise_parser
from mpsd_software_manager.config import MPSD_SOFTWARE_ROOT_MARKER, Config
from mpsd_software_manager.status import status


def test_status_wrong_directory(
    cd: Callable[[Path], AbstractContextManager[None]],
    tmp_path: Path,
    caplog: pytest.LogCaptureFixture,
) -> None:
    """Calling status outside a MPSD software root fails."""
    assert not (tmp_path / MPSD_SOFTWARE_ROOT_MARKER).exists()
    Config(tmp_path)

    with cd(tmp_path), pytest.raises(SystemExit):
        status(initialise_parser().parse_args(["status"]))
    assert "Call to 'status' outside" in caplog.text


def test_status_empty(
    manual_init: Callable[[Path], None],
    cd: Callable[[Path], AbstractContextManager[None]],
    tmp_path: Path,
    caplog: pytest.LogCaptureFixture,
) -> None:
    """Test status command in empty MPSD software directory."""
    manual_init(tmp_path)
    caplog.set_level(logging.INFO)

    with cd(tmp_path):
        status(initialise_parser().parse_args(["status"]))
    assert f"MPSD software root: '{tmp_path}'" in caplog.text
    assert "No software release is installed" in caplog.text


def test_status_releases(
    manual_init: Callable[[Path], None],
    cd: Callable[[Path], AbstractContextManager[None]],
    tmp_path: Path,
    caplog: pytest.LogCaptureFixture,
) -> None:
    """Test reporting of available software releases with fake data."""
    manual_init(tmp_path)
    caplog.set_level(logging.INFO)

    # fake mpsd software releases by creating microarch dirs
    fake_releases = ["develop", "24a"]
    for dir in fake_releases:
        (tmp_path / dir).mkdir()

    with cd(tmp_path):
        status(initialise_parser().parse_args(["status"]))

    assert f"MPSD software root: '{tmp_path}'" in caplog.text
    assert "Installed MPSD software releases:" in caplog.text
    for release in fake_releases:
        assert release in caplog.text


def test_status_package_sets(
    manual_init: Callable[[Path], None],
    cd: Callable[[Path], AbstractContextManager[None]],
    tmp_path: Path,
    caplog: pytest.LogCaptureFixture,
) -> None:
    """Test reporting of available package sets with fake data."""
    manual_init(tmp_path)
    caplog.set_level(logging.INFO)

    # fake mpsd software release
    release = "develop"
    fake_microarches = ["sandybridge", "zen4"]
    extra_dirs = ["logs", "spack-environments"]
    for dir_name in fake_microarches + extra_dirs:
        (tmp_path / release / dir_name).mkdir(parents=True)

    # no lmod directories -> no successful installation

    with cd(tmp_path):
        status(initialise_parser().parse_args(["status", release]))

    for dir_name in fake_microarches:
        assert dir_name in caplog.text
    for extra_dir in extra_dirs:
        assert extra_dir not in caplog.text

    assert "foss2023a-serial" not in caplog.text
    assert "foss2023a-mpi" not in caplog.text
    assert "<No toolchains are fully installed.>" in caplog.text

    caplog.clear()

    # one toolchain module -> partial installation
    toolchain_dir = tmp_path / release / "zen4" / "lmod" / "Core" / "toolchain"
    toolchain_dir.mkdir(parents=True)
    (toolchain_dir / "foss2023a-serial.lua").touch()

    with cd(tmp_path):
        status(initialise_parser().parse_args(["status", release]))

    assert "foss2023a-serial" in caplog.text
    assert "foss2023a-mpi" not in caplog.text
    assert "<No toolchains are fully installed.>" in caplog.text

    caplog.clear()

    # toolchain modules in all releases -> full installation

    toolchain_dir = tmp_path / release / "zen4" / "lmod" / "Core" / "toolchain"
    (toolchain_dir / "foss2023a-mpi.lua").touch()

    toolchain_dir = tmp_path / release / "sandybridge" / "lmod" / "Core" / "toolchain"
    toolchain_dir.mkdir(parents=True)
    (toolchain_dir / "foss2023a-mpi.lua").touch()

    with cd(tmp_path):
        status(initialise_parser().parse_args(["status", release]))

    assert caplog.text.count("foss2023a-serial") == 1
    assert caplog.text.count("foss2023a-mpi") == 2
    assert "<No toolchains are fully installed.>" not in caplog.text


def test_status_packages(
    manual_init: Callable[[Path], None],
    cd: Callable[[Path], AbstractContextManager[None]],
    tmp_path: Path,
    caplog: pytest.LogCaptureFixture,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test reporting of installed packages with fake data."""
    manual_init(tmp_path)
    with cd(tmp_path), pytest.raises(SystemExit):
        status(initialise_parser().parse_args(["status", "develop", "global"]))

    def fake_spack(command: str) -> sp.CompletedProcess[str]:
        if "invalid_set" in command:
            raise sp.CalledProcessError(1, command)
        stdout = "package1\npackage2\n\npackage3"
        return sp.CompletedProcess(command, returncode=0, stdout=stdout)

    monkeypatch.setattr(status_module, "spack", fake_spack)

    caplog.set_level(logging.INFO)
    caplog.clear()

    with cd(tmp_path):
        status(initialise_parser().parse_args(["status", "develop", "foss2023a"]))

    for i in range(1, 4):
        assert f"package{i}" in caplog.text

    with cd(tmp_path), pytest.raises(SystemExit):
        status(initialise_parser().parse_args(["status", "develop", "invalid_set"]))
