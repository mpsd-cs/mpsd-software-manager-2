"""Tests for functions in util and __init__."""

import sys

import mpsd_software_manager as mpsd_software_manager  # required to set sys.excepthook
import pytest


def test_silent_keyboard_interrupt(
    caplog: pytest.LogCaptureFixture, capsys: pytest.CaptureFixture[str]
) -> None:
    """Test that the stacktrace is hidden for keyboard interrupt."""
    # mpsd_software_manager overwrites sys.excepthook
    assert sys.excepthook != sys.__excepthook__

    with pytest.raises(KeyboardInterrupt) as exc_info:
        raise KeyboardInterrupt()

    sys.excepthook(exc_info.type, exc_info.value, None)
    assert "Keyboard interrupt" in caplog.text
    assert "Traceback" not in capsys.readouterr().err


def test_stacktrace_for_other_exceptions(capsys: pytest.CaptureFixture[str]) -> None:
    """Test that the stacktrace is shown for other exceptions."""
    # mpsd_software_manager overwrites sys.excepthook
    assert sys.excepthook != sys.__excepthook__

    with pytest.raises(RuntimeError) as exc_info:
        raise RuntimeError()

    sys.excepthook(exc_info.type, exc_info.value, None)
    assert "Traceback" in capsys.readouterr().err
