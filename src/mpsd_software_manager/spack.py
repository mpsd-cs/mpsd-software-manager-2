"""Wrapper module for spack."""

from __future__ import annotations

import asyncio
import enum
import os
import re
import secrets
import shlex
import shutil
import subprocess as sp
from logging import getLogger
from pathlib import Path
from typing import Any, Callable

import jinja2
import yaml

from .config import Config
from .util import abort

logger = getLogger(__name__)


class MirrorState(enum.Enum):
    """Enum of possible states of a source (binary) mirror."""

    READ_ONLY = "read_only"
    READ_WRITE = "read_write"
    DISABLED = "disabled"


def bootstrap() -> None:
    """Set local bootstrap root and run spack bootstrap."""
    Config().spack_user_cache_root.mkdir(exist_ok=True)
    logger.info("spack: bootstrapping")
    logger.debug("bootstrap root:")
    spack(
        f"bootstrap root --scope site {Config().spack_user_cache_root}",
        log_callback=logger.debug,
    )
    spack("bootstrap now", log_callback=logger.debug)


def find_system_compiler(requested_system_compiler: str | None) -> None:
    """Auto-detect system compiler or use user-specified compiler."""
    logger.info("spack: detecting system compiler")
    spack("compiler find --scope site", log_callback=logger.debug)
    spack_compilers = spack("compiler list", logger.debug).stdout
    config = Config()
    system_compilers = []
    for compiler in re.findall(r"\n(\w+@[0-9.]+)", spack_compilers):
        compiler_info = spack(f"compiler info {compiler}").stdout
        if (
            re.search(rf"\n\s*cc\s*=\s*{config.spack_root!s}", compiler_info)
            is not None
        ):
            # compiler path: cc = /path/to/spack/root -> no system compiler
            continue
        system_compilers.append(compiler)

    if len(system_compilers) == 0:
        abort("Could not find a system compiler.")
    elif (
        requested_system_compiler is not None
        and requested_system_compiler not in system_compilers
    ):
        abort(
            f"Requested system compiler '{requested_system_compiler}' is not known"
            " to Spack"
        )
    elif requested_system_compiler is None and len(system_compilers) > 1:
        abort(
            f"Found multiple system compilers: '{system_compilers}'; specify preferred"
            " system compiler on command line"
        )

    if requested_system_compiler is not None:
        logger.debug(
            "Using '%s' as system compiler per user request", requested_system_compiler
        )
        config.system_compiler = requested_system_compiler
    else:
        system_compiler = system_compilers[0]
        logger.debug("Found system compiler '%s'", system_compiler)
        config.system_compiler = system_compiler


def update_custom_spack_config() -> None:
    """Apply custom spack configuration from spack-environments repository."""
    spack_overlay_root = Config().spack_environments_root / "spack_overlay"
    assert spack_overlay_root.is_absolute()
    files = [
        elem.relative_to(spack_overlay_root)
        for elem in spack_overlay_root.rglob("*")
        if elem.is_file()
    ]
    # source_cache_root may not exist; as fallbacks we try to use the binary cache
    # or a suitable location inside spack
    if Config().source_cache_root.is_dir():
        source_cache = Config().source_cache_root
    elif Config().binary_cache_root.is_dir():
        source_cache = Config().binary_cache_root
    else:
        source_cache = Config().spack_root / "var" / "spack" / "cache"

    for template_file in files:
        render_template(
            template_dir=spack_overlay_root / template_file.parent,
            template_name=template_file.name,
            dest_dir=Config().spack_root / template_file.parent,
            source_cache=source_cache,
            spack_root=Config().spack_root,
            lmod_root=Config().lmod_root,
            system_compiler=Config().system_compiler,
        )


def configure_spack_mirrors(disable_binary_cache: bool) -> None:
    """Configure source and binary cache."""
    add_mirror("source", "mpsd_spack_sources", Config().source_cache_root)
    if not disable_binary_cache:
        Config().binary_cache_root.mkdir(exist_ok=True)
        add_mirror("binary", "local_binary_cache", Config().binary_cache_root)

    # update caches as a safety if the previous call failed
    update_caches(disable_binary_cache)


def update_caches(disable_binary_cache: bool) -> None:
    """Update spack caches to ensure all packages are present and indices up-to-date."""
    # The cache updates are not really required (everything is uploaded to the cache
    # automatically). However the mirror/buildcache indices are not updated
    # automatically.
    installed_packages = spack("find --no-groups -H").stdout.replace("\n", " ")
    if installed_packages == "":
        logger.debug("no packages are installed -> nothing to push to the caches")
        return
    if not disable_binary_cache:
        logger.debug("updating binary cache")
        proc = spack(
            f"buildcache push --private local_binary_cache {installed_packages}"
        )
        logger.debug(proc.stdout.strip())
        proc = spack("buildcache update-index local_binary_cache")
        logger.debug(proc.stdout.strip())
    else:
        logger.debug("Skipping binary cache upload; binary cache is disabled.")

    if get_mirror_state(Config().source_cache_root) == MirrorState.READ_WRITE:
        logger.debug("updating source mirror")
        source_cache = Config().source_cache_root
        proc = spack(f"mirror create --private -d {source_cache} {installed_packages}")
        logger.debug(proc.stdout.strip())
    else:
        logger.debug(
            f"Skipping source cache upload to {Config().source_cache_root};"
            " Cache is read-only."
        )


def render_template(
    template_dir: Path, template_name: str, dest_dir: Path, **context: Any
) -> None:
    """Render given template and write output to dest_dir.

    The template must have name "name.ext.jinja"; it is written to "dest_dir/name.ext".

    All keyword arguments are passed to the template engine as 'context'. Jinja ignores
    unused elements in the context so passing arguments that are not used in a template
    is fine. Likewise, missing variables do not result in an error but are just empty
    strings in the output.

    The guaranteed context is documented in:
    https://gitlab.gwdg.de/mpsd-cs/spack-environments/-/tree/develop/docs/templating.org

    Existing files are overwritten without notice.
    """
    dest_file = template_name.removesuffix(".jinja")
    logger.debug(
        "Rendering template '%s' to '%s'\nContext: %s",
        template_dir / template_name,
        dest_dir / dest_file,
        context,
    )
    env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir))
    template = env.get_template(template_name)
    dest_dir.mkdir(parents=True, exist_ok=True)
    with open(dest_dir / dest_file, "w") as f:
        f.write(template.render(context))


def add_mirror(type_: str, name: str, path: Path) -> None:
    """Add mirror with given name if it is not yet configured and the path exists."""
    if not path.is_dir():
        logger.warning("Ignoring mirror '%s', '%s' is not a directory", name, path)
        return
    existing_mirrors = spack("mirror list").stdout
    if name in existing_mirrors:
        logger.debug("Mirror '%s' is already configured; skipping", name)
        return
    autopush = "--autopush" if get_mirror_state(path) == MirrorState.READ_WRITE else ""
    spack(f"mirror add --type {type_} {autopush} --scope site --unsigned {name} {path}")
    logger.debug(
        "'%s' mirror added (%s): name '%s', path '%s'", type_, autopush, name, path
    )


def get_mirror_state(path: Path) -> MirrorState:
    """Report access permissions of mirror in 'path'."""
    # TODO writing a test file might be safer
    if not path.exists():
        state = MirrorState.DISABLED
    if os.access(path, os.W_OK) and write_test(path):
        state = MirrorState.READ_WRITE
    elif os.access(path, os.R_OK):
        state = MirrorState.READ_ONLY
    else:
        state = MirrorState.DISABLED

    logger.debug("Mirror state of '%s': '%s'", path, state)
    return state


def write_test(path: Path) -> bool:
    """Test if `path` is writable.

    Creates and removes a temporary file and a temporary subdirectory.
    """
    test_file = path / f"tmpfile_{secrets.token_hex(16)}"
    test_dir = path / f"tmpdir_{secrets.token_hex(16)}"
    try:
        test_file.touch(exist_ok=False)
        test_dir.mkdir()
    except PermissionError:
        return False
    else:
        test_file.unlink()
        test_dir.rmdir()
        return True


def install_package_set(package_set: str) -> None:
    """Install one package set."""
    if not (Config().spack_environments_root / "toolchains" / package_set).is_dir():
        abort(
            f"Package set '{package_set}' does not exits in "
            f"{Config().spack_environments_root / 'toolchains'}"
        )

    if "global" in package_set:
        install_package_set_from_file(package_set)
    elif package_set == "miniforge3":
        install_package_set_from_file(package_set)
        post_process_conda(package_set)
    else:
        install_package_set_from_environment(package_set)


def install_package_set_from_file(package_set: str) -> None:
    """Install list of packages globally."""
    package_list = (
        Config().spack_environments_root
        / "toolchains"
        / package_set
        / "global_packages.list"
    )
    logger.info("Installing packages from %s", package_list)
    with package_list.open() as f:
        package_str = f.read()

    packages = []
    for package in package_str.replace("\\\n", "").split("\n"):
        if package == "" or package.startswith("#"):
            continue
        package = package.strip()  # remove any whitespace
        if "%" in package:
            packages.append(package)
        else:
            packages.append(f"{package}%{Config().system_compiler}")

    for package in packages:
        spack_install_package(package)

    refresh_modules()


def spack_install_package(package: str) -> None:
    """Install 'package' if it is not yet installed."""
    if spack(f"find {package}", check_call=False).returncode == 0:
        logger.debug("package '%s' already installed", package)
        return
    logger.info("installing '%s'", package)
    try:
        spack_stream_stdout(
            f"install {package}",
            log_callbacks={
                "==> Installing": logger.info,
                "==> Error: ProcessError": logger.error,
                "==> Error": logger.warning,
                "": logger.debug,
            },
        )
    except sp.CalledProcessError as e:
        raise SpackInstallationError() from e


def refresh_modules(compilers: dict[str, Any] | None = None) -> None:
    """Create lmod modules and change family of intel compiler modules."""
    CLASSIC_FAMILY = "intel_classic_compiler"
    ONEAPI_FAMILY = "intel_oneapi_compiler"

    logger.info("refreshing lmod modules")
    try:
        spack("module lmod refresh -y", log_callback=logger.debug)
    except sp.CalledProcessError as e:
        raise ModuleGenerationError() from e

    # patch intel module
    # - to allow loading gcc and intel compiler simultaneously, we replace
    #   'family("compiler")' in the intel/oneapi lmod files
    # - add gcc as dependent module to intel compilers
    to_patch_file = Config().spack_root / "etc" / "mpsd_intel_module_patching.yaml"

    # record new intel module(s) to patch
    if compilers and "intel" in compilers["default"]["package"]:
        try:
            with open(to_patch_file) as f:
                modules_to_patch = yaml.load(f, Loader=yaml.Loader)
        except FileNotFoundError:
            modules_to_patch = {}

        intel_module = (
            compilers["default"]["package"].split("%")[0].replace("@", "/") + ".lua"
        )
        gcc_module = compilers["fallback"]["package"].split("%")[0].replace("@", "/")
        module_file = Config().lmod_root / "Core" / intel_module
        family = CLASSIC_FAMILY if "classic" in intel_module else ONEAPI_FAMILY

        modules_to_patch[str(module_file)] = {
            "family": family,
            "gcc_module": gcc_module,
        }

        if family == "intel_classic_compiler":
            # We need to also patch the intel module, which is loaded as a dependency of
            # the intel classic module. We read the module name from the intel classic
            # module file.
            classic_content = module_file.read_text()
            match = re.search(
                r'depends_on\("(intel-oneapi-compilers/[0-9.]+)"\)', classic_content
            )
            # match.group(1) will fail should we not find the line (should never happen)
            try:
                oneapi_module = match.group(1) + ".lua"
            except (AttributeError, IndexError) as e:
                logger.error(
                    "Failed to find 'depends_on(intel-oneapi-compilers/...)' in '%s'",
                    module_file,
                    classic_content,
                )
                raise ModuleGenerationError() from e
            oneapi_module_file = Config().lmod_root / "Core" / oneapi_module
            modules_to_patch[str(oneapi_module_file)] = {
                "family": ONEAPI_FAMILY,
                "gcc_module": gcc_module,
            }

        with open(to_patch_file, "w") as f:
            yaml.dump(modules_to_patch, f)

    # patch all intel modules
    try:
        with open(to_patch_file) as f:
            modules_to_patch = yaml.load(f, Loader=yaml.Loader)
    except FileNotFoundError:
        logger.debug("No intel modules to patch")
        return

    for module_file in modules_to_patch:
        gcc_module = modules_to_patch[module_file]["gcc_module"]
        family = modules_to_patch[module_file]["family"]
        patch_intel_module(Path(module_file), gcc_module, family)


def patch_intel_module(module_file: Path, gcc_module: str, family: str) -> None:
    """Make intel modules depend on gcc and change family."""
    if not module_file.exists():
        logger.warning("Module '%s' does not exist; skipping", module_file)
    logger.debug("Updating family(...) in '%s'", module_file)
    content = module_file.read_text()
    content = content.replace('family("compiler")', f'family("{family}")')
    # Insert gcc as dependency before other dependencies and before modifying
    # MODULE_PATH.
    # This function can be called multiple times, so only insert if not yet present.
    if f'depends_on("{gcc_module}")\n' not in content:
        logger.debug("Inserting dependency on '%s' in '%s'", gcc_module, module_file)
        insertion_point = min(
            content.find("depends_on("), content.find('prepend_path("MODULEPATH"')
        )
        content = (
            content[:insertion_point]
            + f'depends_on("{gcc_module}")\n'
            + content[insertion_point:]
        )
    module_file.write_text(content)


def install_package_set_from_environment(spack_environment: str) -> None:
    """Install packages in Spack environment."""
    compilers = install_toolchain_compiler(spack_environment)

    spack_environments = spack("env list").stdout.split()
    if spack_environment not in spack_environments:
        logger.debug(spack(f"env create {spack_environment}"))

    config = Config()
    context = {
        "toolchain_compiler": compilers["default"]["name"],
        # cuda arch: avocado (61), gpu (70), gpu-ayyer (70), accelerated tentacles (75)
        "cuda_arch": "61,70,75",
    }
    if "fallback" in compilers:
        context["fallback_compiler"] = compilers["fallback"]["name"]
    render_template(
        config.spack_environments_root / "toolchains" / spack_environment,
        "spack.yaml.jinja",
        spack_environment_path(spack_environment),
        **context,
    )

    logger.info("concretizing environment '%s'", spack_environment)
    try:
        logger.debug(spack(f"-e {spack_environment} concretize --force").stdout)
    except sp.CalledProcessError as e:
        raise SpackConcretizationError() from e

    logger.info("installing environment '%s'", spack_environment)
    try:
        spack_stream_stdout(
            f"-e {spack_environment} install",
            log_callbacks={
                "==> Installing": logger.info,
                "==> Error: ProcessError": logger.error,
                "==> Error": logger.warning,  # summary at the end of the installation
                "": logger.debug,
            },
        )
    except sp.CalledProcessError as e:
        raise SpackInstallationError() from e

    refresh_modules(compilers)
    generate_meta_modules(spack_environment, compilers)


def spack_environment_path(spack_environment: str) -> Path:
    """Path to spack environment directory inside spack root."""
    return Config().spack_root / "var" / "spack" / "environments" / spack_environment


def generate_meta_modules(
    spack_environment: str, compilers: dict[str, dict[str, str]]
) -> None:
    """Generate two meta-modules for the spack environment."""
    logger.info("generating lmod metamodules for '%s'", spack_environment)
    spack(f"-e {spack_environment} env loads -m lmod", log_callback=logger.debug)
    with (spack_environment_path(spack_environment) / "loads").open() as f:
        loads_file = f.read()

    MPI_MODULES = ["openmpi", "intel-oneapi-mpi"]
    GENERIC_TOOLCHAIN_MODULES = [  # deprecated
        "binutils",
        "fftw",
        "openblas",
        "autoconf",
        "libtool",
        "automake",
        "netlib-scalapack",
        "cmake",
        "ninja",
        "intel-oneapi-mkl",
    ]

    # Not all dependencies are available in all toolchains (e.g. mkl only in intel,
    # scalapack only in GCC+MPI, etc.). The correct subset is picked based on the
    # packages available in a spack environment. This list contains all packages that
    # are required for any version of a min build. Packages that are optionally
    # downloaded via FetchContent (e.g. libxc) MUST NOT be included.
    OCTOPUS_DEPENDENCIES_MIN = [
        "fftw",
        "openblas",
        "netlib-scalapack",
        "intel-oneapi-mkl",
        "cmake",
        "ninja",
        "gsl",
        "cuda",
        "perl-yaml",
    ]

    SKIP = [
        "berkeleygw/4.0",  # not yet compatible with octopus
    ]

    toolchain_pre = []  # collect packages for hierarchy: compilers and mpi
    mpi_package = None
    toolchain_packages = []
    octopus_dependencies = []
    octopus_dependencies_min = []

    if "fallback" in compilers:
        toolchain_pre.append(
            compilers["fallback"]["package"].replace("@", "/").split("%")[0]
        )
    toolchain_pre.append(
        compilers["default"]["package"].replace("@", "/").split("%")[0]
    )

    # format of the lines: "module load PACKAGE/VERSION"
    for line in loads_file.split("\n"):
        if not line.startswith("module load"):
            continue
        # format: "module load package/version"
        package = line.split()[-1]
        if package.split("/")[0] in MPI_MODULES:
            mpi_package = package.replace("/", "@")
            if "cuda" in mpi_package:
                # with cuda the name is openmpi@version-cuda-cudaversion;
                # we need to convert to openmpi@version+cuda
                mpi_package = mpi_package.split("-")[0] + "+cuda"
            elif "openmpi" in mpi_package:
                # explicitly ask for the non-cuda variant to not find multiple
                # candidates when mpi+cuda is also installed
                mpi_package = mpi_package + "~cuda"
            # intel mpi does not have cuda support
            toolchain_pre.append(package)
        elif package.split("/")[0] in OCTOPUS_DEPENDENCIES_MIN:
            octopus_dependencies_min.append(package)
            octopus_dependencies.append(package)
        elif package in SKIP:
            continue
        else:
            octopus_dependencies.append(package)

        # DEPRECATED
        if package.split("/")[0] in GENERIC_TOOLCHAIN_MODULES:
            toolchain_packages.append(package)

    TOOLCHAIN_NAME_MAPPING = {
        "gcc-11_5_0": "foss2022a-serial",
        "gcc-11_5_0-openmpi-4_1_4": "foss2022a-mpi",
        "gcc-11_5_0-openmpi-4_1_4-cuda-11_4": "foss2022a-cuda-mpi",
        "gcc-12_3_0": "foss2023a-serial",
        "gcc-12_3_0-openmpi-4_1_5": "foss2023a-mpi",
        "gcc-13_2_0": "foss2023b-serial",
        "gcc-13_2_0-openmpi-4_1_6": "foss2023b-mpi",
        "intel-2021_6_0": "intel2022a-serial",
        "intel-2021_6_0-impi-2021_6_0": "intel2022a-mpi",
        "intel-2021_9_0": "intel2023a-serial",
        "intel-2021_9_0-impi-2021_9_0": "intel2023a-mpi",
    }

    toolchain_module_name = TOOLCHAIN_NAME_MAPPING.get(spack_environment)
    if toolchain_module_name:
        logger.debug("toolchain metamodule: '%s'", toolchain_module_name)
        logger.debug("toolchain packages: '%s'", toolchain_pre + toolchain_packages)

        hierarchy_modules = " ".join(toolchain_pre)
        toolchain_deprecation_warning = (
            f"The toolchain modules are deprecated; load {hierarchy_modules} and other"
            " modules explicitly. For compiling octopus: the octopus-dependencies"
            " module in variants min (full) contains all required (required and"
            f" optional) dependencies. Use: 'module load {hierarchy_modules}"
            " octopus-dependencies/VARIANT'"
        )
        write_lua_module(
            Config().lmod_root / "Core" / "toolchain" / f"{toolchain_module_name}.lua",
            toolchain_pre + toolchain_packages,
            toolchain_deprecation_warning,
        )
    else:
        logger.debug(
            "No deprecated toolchain metamodule is created for '%s'", spack_environment
        )

    logger.debug("octopus dependencies min: '%s'", octopus_dependencies_min)
    logger.debug("octopus dependencies: '%s'", octopus_dependencies)

    compiler_module = compilers["default"]["name"].replace("@", "/")
    if mpi_package is None:
        # required format: /.../lmod/gcc/11.3.0/<module-name>
        # or: /.../lmod/intel/2021.9.0/<module-name>
        lmod_octopus_dependencies_root = (
            Config().lmod_root / compiler_module / "octopus-dependencies"
        )
    else:
        # required format: /.../lmod/openmpi/4.1.4-7imdm7p/gcc/11.3.0/<module-name>
        mpi_module = spack(
            f'find --format "{{name}}/{{version}}-{{hash:7}}" {mpi_package}'
        ).stdout.strip()
        lmod_octopus_dependencies_root = (
            Config().lmod_root / mpi_module / compiler_module / "octopus-dependencies"
        )

    if "cuda" in spack_environment:
        min_name = "min-cuda"
        full_name = "full-cuda"
    else:
        min_name = "min"
        full_name = "full"

    write_lua_module(
        lmod_octopus_dependencies_root / f"{min_name}.lua",
        octopus_dependencies_min,
        help_msg="Required dependencies to compile octopus.",
    )
    write_lua_module(
        lmod_octopus_dependencies_root / f"{full_name}.lua",
        octopus_dependencies,
        help_msg="Required and optional dependencies to compile octopus.",
        default=True,
    )


def write_lua_module(
    file_path: Path,
    modules: list[str],
    warning: str | None = None,
    help_msg: str | None = None,
    default: bool = False,
) -> None:
    """Write lua metamodule with a list of dependencies; create directory if needed."""
    logger.debug("writing lmod module file '%s'", file_path)
    file_path.parent.mkdir(exist_ok=True)
    with file_path.open("w") as f:
        f.write("-- -*- lua -*-\n")
        f.write("\n".join(f'depends_on("{module}")' for module in modules) + "\n")
        if help_msg:
            f.write(f"help([[{help_msg}]])")
        if warning:
            f.write(f'LmodWarning("{warning}")\n')

    if default:
        default_path = file_path.parent / "default"
        if default_path.is_symlink():
            default_path.unlink()
        default_path.symlink_to(file_path)


def install_toolchain_compiler(spack_environment: str) -> dict[str, dict[str, str]]:
    """Install toolchain compilers."""
    # TODO replace compiler file with something structured like yaml
    logger.info("installing toolchain compiler")
    with (
        Config().spack_environments_root
        / "toolchains"
        / spack_environment
        / "compiler_vars.sh"
    ).open() as f:
        compiler_vars = f.read()
    compilers = {}
    if compiler_vars.startswith("toolchain_compiler_package"):
        # format: toolchain_compiler_package="gcc@12.3.0"
        toolchain_compiler = compiler_vars.split("\n")[0].split('"')[1]
        compilers["default"] = {
            "name": toolchain_compiler,
            "package": f"{toolchain_compiler}%{Config().system_compiler}",
        }
    else:
        # format (may contain additional blank lines):
        # intel_version="2021.9.0"
        # toolchain_compiler_package="intel-oneapi-compilers-classic@${intel_version}"
        # export TOOLCHAIN_COMPILER="intel@${intel_version}"
        # unset intel_version
        # toolchain_gcc_package="gcc@12.3.0"
        # export TOOLCHAIN_GCC=$toolchain_gcc_package
        for line in compiler_vars.split("\n"):
            if line.startswith("intel_version"):
                intel_version = line.split('"')[1]
            elif line.startswith("toolchain_compiler_package"):
                intel_package = line.split('"')[1].replace(
                    "${intel_version}", intel_version
                )
            elif line.startswith("export TOOLCHAIN_COMPILER"):
                intel_compiler = line.split('"')[1].replace(
                    "${intel_version}", intel_version
                )
            elif line.startswith("toolchain_gcc_package"):
                gcc_package = line.split('"')[1]
        compilers["fallback"] = {
            "name": gcc_package,
            "package": f"{gcc_package}%{Config().system_compiler}",
        }
        compilers["default"] = {
            "name": intel_compiler,
            "package": f"{intel_package}%{gcc_package}",
        }

    logger.debug("Required toolchain compiler: %s", compilers)

    for compiler_definition in compilers.values():
        compiler_package = compiler_definition["package"]
        spack_install_package(compiler_package)
        compiler_path = spack(f"location -i {compiler_package}").stdout.strip()
        if compiler_package.startswith("intel-oneapi-compilers@"):
            # convert 'intel-oneapi-compilers@2025.0.0%gcc@14.2.0' to '2025.0'
            # assumption: the path always contains major.minor
            version = ".".join(
                compiler_package.split("%")[0].split("@")[1].split(".")[:2]
            )
            compiler_path += f"/compiler/{version}"
        logger.debug("Checking for compiler in '%s'", compiler_path)
        logger.debug(
            spack(f"compiler find --scope site {compiler_path}").stdout.strip()
        )

    return compilers


class SpackConcretizationError(Exception):
    """Exception to indicate a failure during the environment concretisation."""


class SpackInstallationError(Exception):
    """Exception to indicate a failure during the Spack installation."""


class ModuleGenerationError(Exception):
    """Exception to indicate a failure during the module file generation."""


class CondaEnvError(Exception):
    """Exception to indicate a failure during the conda env creation."""


def spack(
    command: str,
    log_callback: Callable[[str], None] | None = None,
    check_call: bool = True,
    **kwargs: Any,
) -> sp.CompletedProcess[str]:
    """Run spack command and capture stdout and stderr.

    Raises an exception if the spack command has non-zero exit code.

    If log_callback is provided stdout will be written to the log after the process has
    completed.
    """
    proc = run_spack_shell_command(
        f"spack {command}",
        env=spack_shell_env(),
        capture_output=True,
        text=True,
        **kwargs,
    )
    if proc.returncode != 0 and check_call:
        logger.error("command 'spack %s' failed", command)
        logger.error("stdout: %s", proc.stdout)
        logger.error("stderr: %s", proc.stderr)
        raise sp.CalledProcessError(proc.returncode, f"spack {command}")
    if log_callback is not None:
        log_callback(proc.stdout)
    return proc


def spack_stream_stdout(
    command: str, log_callbacks: dict[str, Callable[[str], None]], **kwargs: Any
) -> None:
    """Run spack command and write stdout to log callback asynchronuously.

    Implementation for asyncio code based on:
    https://kevinmccarthy.org/2016/07/25/streaming-subprocess-stdin-and-stdout-with-asyncio-in-python/
    """
    loop = asyncio.get_event_loop()
    returncode = loop.run_until_complete(
        run_async_spack_shell_command(
            f"spack {command}",
            log_callbacks=log_callbacks,
            env=spack_shell_env(),
            **kwargs,
        )
    )
    if returncode != 0:
        logger.error("command 'spack %s' failed", command)
        raise sp.CalledProcessError(returncode, f"spack {command}")


def run_spack_shell_command(
    command: str, env: dict[str, str] | None = None, **kwargs: Any
) -> sp.CompletedProcess[str]:
    """Execute 'command' in the spack root directory using bash with a custom shell env.

    Keyword arguments are passed on to subprocess.run().
    """
    return sp.run(
        command,
        shell=True,
        executable="/usr/bin/bash",
        env=env,
        cwd=Config().spack_root,
        **kwargs,
    )


async def run_async_spack_shell_command(
    command: str,
    log_callbacks: dict[str, Callable[[str], None]],
    env: dict[str, str] | None = None,
    **kwargs: Any,
) -> int:
    """Execute 'command' in the spack root directory using bash with a custom shell env.

    Keyword arguments are passed on to subprocess.run().
    """
    process = await asyncio.create_subprocess_shell(
        command,
        shell=True,
        executable="/usr/bin/bash",
        env=env,
        cwd=Config().spack_root,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.STDOUT,
        **kwargs,
    )
    assert isinstance(process.stdout, asyncio.StreamReader)
    await async_log_output(process.stdout, log_callbacks)
    return await process.wait()


async def async_log_output(
    stream: asyncio.StreamReader, log_callbacks: dict[str, Callable[[str], None]]
) -> None:
    """Write lines of stream to suitable log_callback.

    A suitable callback is selected based on the keys of the log_callbacks dict, which
    must match the line beginnings.
    """
    while not stream.at_eof():
        line = (await stream.readline()).decode()
        if line:
            for prefix in log_callbacks:
                if line.startswith(prefix):
                    log_callbacks[prefix](line.strip())
                    break


def spack_shell_env() -> dict[str, str]:
    """Shell environment with Spack in PATH and required environment variables set.

    Additional environment variables are required to fully isolate Spack
    (using --scope site is not sufficient). Details at:
    https://spack.readthedocs.io/en/latest/configuration.html#overriding-local-configuration

    """
    env = os.environ.copy()
    env.update(
        {
            "PATH": f"{str(Config().spack_root / 'bin')}:{env['PATH']}",
            "SPACK_ROOT": str(Config().spack_root),
            "SPACK_DISABLE_LOCAL_CONFIG": "true",
            "SPACK_USER_CACHE_PATH": str(Config().spack_user_cache_root),
        }
    )
    return env


def ensure_no_active_spack() -> None:
    """Terminate program if a spack instance is currently active."""
    if (active_spack := shutil.which("spack")) is not None:
        abort(
            f"Found active spack instance '{active_spack}';"
            " rerun without spack in PATH."
        )


def post_process_conda(package_set: str) -> None:
    """Adjust conda installation to MPSD requirements.

    Copy .condarc and create global conda environment based on yaml file distributed
    with the miniforge3 package set.

    """
    logger.info("Creating default conda environment")
    pkg_src_dir = Config().spack_environments_root / "toolchains" / package_set
    pkg_install_dir = Path(spack("location -i miniforge3").stdout.strip())

    shutil.copyfile(pkg_src_dir / "dot_condarc", pkg_install_dir / ".condarc")

    yml_list = list(pkg_src_dir.glob("*.yaml"))
    if len(yml_list) > 1:
        logger.warning(
            "Found multiple candidates for the conda env file: '%s'."
            "All but the first will be ignored",
            yml_list,
        )
    command = [
        str(pkg_install_dir / "bin" / "conda"),
        "env",
        "update",
        "--prune",
        "--file",
        str(yml_list[0]),
    ]
    logger.debug("Running '%s'", shlex.join(command))
    proc = sp.run(
        command,
        capture_output=True,
        text=True,
    )
    if proc.returncode != 0:
        logger.error(
            "Conda env creation failed:\nstdout:\n%s\n\nstderr:\n%s",
            proc.stdout,
            proc.stderr,
        )
        raise CondaEnvError()
    else:
        logger.debug(proc.stdout)
