"""Custom pytest fixtures."""

import contextlib
import os
from collections.abc import Iterator
from contextlib import AbstractContextManager
from pathlib import Path
from typing import Callable

import pytest
from mpsd_software_manager.config import MPSD_SOFTWARE_ROOT_MARKER, Config


@pytest.fixture(autouse=True)
def reset_config(request: pytest.FixtureRequest) -> None:
    """Reset config object to ensure tests are independent.

    If a test is marked with 'default_config' initialise config using the current
    working directory.

    """
    Config._instance = None
    if "default_config" in request.keywords:
        Config(Path.cwd())


@pytest.fixture
def cd() -> Callable[[Path], AbstractContextManager[None]]:
    """Context manager to temporarily change working directories."""

    @contextlib.contextmanager
    def _cd(directory: Path) -> Iterator[None]:
        assert directory.is_dir()
        old_working_directory = Path.cwd()
        os.chdir(directory)
        try:
            yield
        finally:
            os.chdir(old_working_directory)

    return _cd


@pytest.fixture
def manual_init() -> Callable[[Path], None]:
    """Initialise MPSD software root manually and instantiate Config."""

    def _manual_init(path: Path) -> None:
        (path / MPSD_SOFTWARE_ROOT_MARKER).touch()
        Config(path)

    return _manual_init
